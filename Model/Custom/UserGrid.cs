﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Custom
{
    public class UserGrid
    {
        public string Id { get; set; }
        public string NameUser { get; set; }
        public string Role { get; set; }

    }
}
