namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class MPermisoRoles
    {
        public int Id { get; set; }

        [StringLength(128)]
        public string IdRole { get; set; }

        public int? IdMenu { get; set; }

        public bool? Active { get; set; }
    }
}
