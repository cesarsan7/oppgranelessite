﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Model
{
    public class ApplicationRole : IdentityRole
        {
            public bool Enable { get; set; }

        }
}
