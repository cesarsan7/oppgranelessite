﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    class UserGrid
    {
        public String Id { get; set; }
        public String NameUser { get; set; }
        public String Role { get; set; }
        public string CodUser { get; set; }
    }
}
