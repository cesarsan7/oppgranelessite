﻿using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System.Net.Http;
using System.Security.Claims;
using System.Web;

namespace Common
{
    public class CurrentUser
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string NameUser { get; set; }
        public string Email { get; set; }
        public string CodUser { get; set; }
        public string Roles { get; set; }

        public static CurrentUser Get
        {
            get
            {
                var user = HttpContext.Current.User;

                if (((ClaimsIdentity)user.Identity).FindFirst(ClaimTypes.UserData) == null)
                    return null;

                if (user == null)
                {
                    return null;
                }
                else if (string.IsNullOrEmpty(user.Identity.GetUserId()))
                {
                    return null;
                }
                
                var jUser = ((ClaimsIdentity)user.Identity).FindFirst(ClaimTypes.UserData).Value;
                return JsonConvert.DeserializeObject<CurrentUser>(jUser);
            }
        }

    }
}
