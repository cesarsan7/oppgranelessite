﻿using Model;
using Model.Custom;
using Persistence;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class UserService
    {
        public IEnumerable<UserGrid> GetAll()
        {
            var result = new List<UserGrid>();

            using (var ctx = new ApplicationDbContext())
            {
                //result = ctx.ApplicationUsers.ToList();
                result = (
                    from au in ctx.ApplicationUsers
                    from aur in ctx.ApplicationUserRoles.Where(x => x.UserId == au.Id).DefaultIfEmpty()
                    from ar in ctx.ApplicationRoles.Where(x => x.Id == aur.RoleId && x.Enable).DefaultIfEmpty()
                    select new UserGrid
                    {
                        Id = au.Id,
                        NameUser = au.NameUser,
                        Role = ar.Name
                    }
                    ).ToList();
            }
            return result;
        }

        public ApplicationUser Get(string id)
        {
            var result = new ApplicationUser();

            using (var ctx = new ApplicationDbContext())
            {
                result = ctx.ApplicationUsers.Where(x => x.Id == id).Single();
            }
            return result;
        }

        public void Update(ApplicationUser model)
        {
            var result = new List<ApplicationUser>();

            using (var ctx = new ApplicationDbContext())
            {
                var originalEntity = ctx.ApplicationUsers.Where(x => x.Id == model.Id).Single();

                originalEntity.NameUser = model.NameUser;
                
                ctx.Entry(model).State = EntityState.Modified;
                ctx.SaveChanges();
            }
            
        }

        //public IEnumerable<Menu1> Get1()
        //{
        //    var result = new List<Menu1>();
            
        //    using (var ctx = new BaseAppContext())
        //    {
        //        result = ctx.Menu1.ToList();
        //    }
        //    return result;
        //}

    }
}
