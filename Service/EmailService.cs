﻿using Microsoft.AspNet.Identity;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Service
{
    public class EmailService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your email service here to send an email.
            //return Task.FromResult(0);

            //SmtpClient client = new SmtpClient();
            //return client.SendMailAsync("notificaiones@grupoalumina.com.co",
            //                            message.Destination,
            //                            message.Subject,
            //                            message.Body
            //                           );
            SmtpClient client = new SmtpClient();
            MailMessage msg = null;
            msg = new MailMessage(
                "notificaiones@grupoalumina.com.co",
                message.Destination,
                message.Subject,
                message.Body);
            msg.IsBodyHtml = true;

            return client.SendMailAsync(msg);
        }
    }
}
