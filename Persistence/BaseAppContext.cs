namespace Model
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class BaseAppContext : DbContext
    {
        public BaseAppContext()
            : base("name=BaseAppContext")
        {
        }

        public virtual DbSet<Menu> Menu { get; set; }
        public virtual DbSet<MPermisoRoles> MPermisoRoles { get; set; }
        public virtual DbSet<vwMPermisos> VwMPermisos { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Menu>()
            //    .Property(e => e.Name)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Menu>()
            //    .Property(e => e.Role)
            //    .IsFixedLength();

            //modelBuilder.Entity<Menu>()
            //    .Property(e => e.Url)
            //    .IsFixedLength();

            //Database.SetInitializer<YourDbContext>(null);
            //base.OnModelCreating(modelBuilder);

            Database.SetInitializer<BaseAppContext>(null);
            base.OnModelCreating(modelBuilder);
        }
    }
}
