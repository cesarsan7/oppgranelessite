﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Model;
using Service;
using Service.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace baseApp2.Controllers
{
    public class UserController : ApplicationBaseController
    {

        private readonly UserService _userService = new UserService();

        private ApplicationRoleManager _roleManager
        {
            get
            {
                return HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
        }

        private ApplicationUserManager _userManager
        {
            get
            {
                return HttpContext.GetOwinContext().Get<ApplicationUserManager>();
            }
        }
        public ActionResult Index()
        {
            return View(
                _userService.GetAll()
                );
        }

        public ActionResult Get(string id)
        {
            ViewBag.Roles = _roleManager.Roles.Where(x=> x.Enable ).ToList();
            return View(
                _userService.Get(id)
                );
        }

        public async Task<ActionResult> AddRoleToUser(string Id, string role)
        {
            var roles = await _userManager.GetRolesAsync(Id);
 
            if (roles.Any())
            {
                throw new Exception("El rol actual ya existe para el Usuario");
            }
            await _userManager.AddToRoleAsync(Id, role);
            return RedirectToAction("Index");
        }


        public async Task CreateRoles()
        {
            var roles = new List<ApplicationRole>
            {
                new ApplicationRole { Name = "Admin"},
                new ApplicationRole { Name = "Moderador"},
                new ApplicationRole { Name = "User"}

            };

            foreach (var r in roles)
            {
                if (!await _roleManager.RoleExistsAsync(r.Name))
                {
                    await _roleManager.CreateAsync(r);
                }
            }
        }

        [AllowAnonymous]
        public string Anonymous()
        {
            return "anonymous";
        }
        [Authorize(Roles = "Guest")]
        public string Guest()
        {
            return "Guest";
        }
        [Authorize(Roles = "Moderator")]
        public string Moderator()
        {
            return "Moderator";
        }
        [Authorize(Roles = "Admin")]
        public string Admin()
        {
            return "Admin";
        }

        //private BaseAppContext db = new BaseAppContext();
        public ActionResult Get1()
        {
           // ViewBag.Menu = this.db.Menu1.Where(x=> x.ParentId == null && x.Active == true).ToList();
           //ViewBag.Menu2 = this.db.Menu1.Where(x => x.ParentId != null && x.Active == true).ToList();
            return View(
               // _userService.Get1()
                );
        }

    }
    }
