﻿using baseApp2.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace baseApp2.Controllers
{
    public class CuadrillaController : Controller
    {
        // GET: Cuadrilla
        public ActionResult Index()
        {
            IEnumerable<CuadrillaModel> cuadrillas = null;
            string Url = "http://localhost:10453/api/Cuadrilla";


        

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:10453/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Cuadrilla");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    //  var readTask = result.Content.ReadAsAsync<IList<CuadrillaModel>>();
                    var readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();
                    //return JsonConvert.DeserializeObject<List<Model>>(content);
                    cuadrillas = JsonConvert.DeserializeObject<List<CuadrillaModel>>(readTask.Result);
                }
                else //web api sent error response 
                {
                    //log response status here..

                    cuadrillas = Enumerable.Empty<CuadrillaModel>();

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View(cuadrillas);
        }


        //public ActionResult Create(CuadrillaModel cuadrilla)
        //{

        //    using (var client = new HttpClient())
        //    {
        //        client.BaseAddress = new Uri("http://localhost:64189/api/Cuadrilla");

        //        //HTTP POST
        //       // var postTask = client.PostAsync(<CuadrillaModel>("Cuadrilla", cuadrilla);
        //        postTask.Wait();

        //        var result = postTask.Result;
        //        if (result.IsSuccessStatusCode)
        //        {
        //            return RedirectToAction("Index");
        //        }
        //    }

        //    ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");

        //    return View(cuadrilla);
        //}
    }
}