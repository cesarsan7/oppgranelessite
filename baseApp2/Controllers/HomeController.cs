﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace baseApp2.Controllers
{
    public class HomeController : ApplicationBaseController
    {

        public ActionResult Index()
        {
            
            return View();
        }

        public ActionResult ReservaTurnos()
        {
            ViewBag.Message = "";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        //public ActionResult ReservaTurnos()
        //{
        //    ViewBag.Message = "Reporte para visualizar vehiculos en reserva de turnos";

        //    return View();
        //}

        public ActionResult Enturnados()
        {
            ViewBag.Message = "";

            return View();
        }

        public ActionResult Radicados()
        {
            ViewBag.Message = "";

            return View();
        }

        public ActionResult Tarados()
        {
            ViewBag.Message = "";

            return View();
        }

        public ActionResult DesTarados()
        {
            ViewBag.Message = "";

            return View();
        }

        public ActionResult TaraVolquetas()
        {
            ViewBag.Message = "";

            return View();
        }

        public ActionResult Muelles()
        {
            ViewBag.Message = "";

            return View();
        }

        public ActionResult Varios()
        {
            ViewBag.Message = "";

            return View();
        }

        public ActionResult Verificados()
        {
            ViewBag.Message = "";

            return View();
        }

        public ActionResult DesTarados2()
        {
            ViewBag.Message = "";

            return View();
        }

        public ActionResult DesTarados1()
        {
            ViewBag.Message = "";

            return View();
        }

        public ActionResult TaraVolquetas2()
        {
            ViewBag.Message = "";

            return View();
        }


        public ActionResult Test()
        {
            ViewBag.Message = "";

            return View();
        }


    }
}