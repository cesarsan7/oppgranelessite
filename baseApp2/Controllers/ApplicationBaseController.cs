﻿using Model;
using Common;
using Persistence;
using System.Linq;
using System.Web.Mvc;

namespace baseApp2.Controllers
{
    public class ApplicationBaseController : Controller
    {
        public BaseAppContext db = new BaseAppContext();
        

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (User != null)
            {
                var context = new ApplicationDbContext();
                var username = User.Identity.Name;

                if (!string.IsNullOrEmpty(username))
                {
                    var user = context.ApplicationUsers.SingleOrDefault(u => u.UserName == username);
                    string fullName = user.NameUser ;
                    ViewData.Add("FullName", fullName);

                    var roles = CurrentUser.Get.Roles;
                    

                        ViewBag.Menu =db.VwMPermisos.Where(x => x.ParentId == null && x.Active == true && x.IdRole == roles).ToList().OrderBy(x=> x.ParentOrder);
                        ViewBag.Menu2 = db.VwMPermisos.Where(x => x.ParentId != null && x.Active == true && x.IdRole == roles).ToList().OrderBy(x => x.MenuOrder);


                }


            }
            base.OnActionExecuted(filterContext);



           
            

        }
        public ApplicationBaseController()
        { }
    }
}