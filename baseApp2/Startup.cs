﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(baseApp2.Startup))]
namespace baseApp2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
