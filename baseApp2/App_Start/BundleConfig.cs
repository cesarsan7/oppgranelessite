﻿using System.Web;
using System.Web.Optimization;

namespace baseApp2
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));


            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        //"~/Scripts/jquery-{version}.js"
                        /*jQuery 3*/
                        "~/bower_components/jquery/dist/jquery-1.12.4.js",
                        "~/bower_components/jquery/dist/jquery.min.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                        /*"~/Scripts/bootstrap.js",*/
                        "~/bower_components/bootstrap/dist/js/bootstrap.min.js"
                        //,"~/Scripts/respond.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/base").Include(
                        "~/dist/js/adminlte.min.js",
                        "~/dist/js/demo.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/dataTables").Include(
                        "~/bower_components/datatables.net/js/jquery.dataTables.min.js",
                        "~/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js",
                        "~/bower_components/jquery-slimscroll/jquery.slimscroll.min.js",
                        "~/bower_components/fastclick/lib/fastclick.js"
                        ));

            
            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                        "~/bower_components/bootstrap/dist/css/bootstrap.min.css",
                        "~/bower_components/font-awesome/css/font-awesome.min.css",
                        "~/bower_components/Ionicons/css/ionicons.min.css",
                        "~/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css",
                        "~/dist/css/AdminLTE.css",
                        "~/dist/css/skins/_all-skins.min.css",
                        "~/dist/css/Custom.css",
                        "~/bower_components/jvectormap/jquery-jvectormap.css"));
        }
    }
}
