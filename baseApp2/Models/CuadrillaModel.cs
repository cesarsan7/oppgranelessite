﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace baseApp2.Models
{
    [Table("Cuadrilla")]
    public class CuadrillaModel 
    {
        [Key]
        [Column("Id")]
        public int? Id { get; set; }

        [Required(ErrorMessage = "Empresa es requerido")]
        public int IdEmpresa { get; set; }

        [Required(ErrorMessage = "Nombre es requerido")]
        [StringLength(50, ErrorMessage = "Nombre no puede ser mayor a 50 caracteres")]
        public string Nombre { get; set; }

        [StringLength(100, ErrorMessage = "Desripción no puede ser mayor a 100 caracteres")]
        public string Descripcion { get; set; }

        [StringLength(2, ErrorMessage = "Activo no puede ser mayor a 2 caracteres")]
        public string Activo { get; set; }

        [StringLength(10, ErrorMessage = "Código no puede ser mayor a 10 caracteres")]
        public string Codigo { get; set; }
    }
}